#!/usr/bin/env bash

apt-get update
sudo debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password password test'
sudo debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password_again password test'
sudo apt-get -y install mysql-server libapache2-mod-auth-mysql php5-mysql
